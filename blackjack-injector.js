const replaceAddEventListener = function () {
	let originalWindowEventListener = EventTarget.prototype.addEventListener;
	EventTarget.prototype.addEventListener = function() {
		//console.log("function called with args", this, arguments);
		originalWindowEventListener.apply(this, arguments);
	};
}

browser.storage.local.get("kat").then(console.log, console.log);
let scriptObj = document.createElement("script");
scriptObj.text = `(${replaceAddEventListener.toString()})()`;
(document.head || document.documentElement).prepend(scriptObj);
