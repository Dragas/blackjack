function generateUUID() { // Public Domain/MIT
    let d = new Date().getTime();//Timestamp
    let d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
function blackjackStoreFilter(filterDefinition) {
	console.log(filterDefinition);
}
function blackjackStoreEvent(event) {
	event.preventDefault();
	const form = event.target;
	const eventFilterDefinition = {
		"name": form.event_name_input.value,
		"domain": form.event_domain_input.value,
		"type": form.event_type_input.value
	}
	if(eventFilterDefinition.name === "") {
		eventFilterDefinition.name = generateUUID();
	}
	if(eventFilterDefinition.domain === "") {
		eventFilterDefinition.domain = "*";
	}
	if(eventFilterDefinition.type === "") {
		eventFilterDefinition.type = "*";
	}
	form.reset();
	blackjackStoreFilter(eventFilterDefinition);
}
(function() {
	let form = document.getElementById("event_form");
	form.addEventListener("submit", blackjackStoreEvent);
})();